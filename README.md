# My ZSH Setup


## Name
Kaj's Personal ZSH setup

## Description
My personal preference for a zsh setup.  This is a script that will automatically install zsh, p10k and my personally preferred plugins.

## Installation
- Clone the git repository and run setup.sh
- Press Y when it asks you to set zsh as your default shell
- This will open zsh, now press ctrl+D to close it and continue the installation
- Now log in again and everything is done!

## Dependencies
- A linux installation that supports apt-get pacakage manager (tested on Debian)
- wget* 
- git* 
- zsh* 


(* Will be installed automatically if not installed yet)

## Support
Feel free to write an issue if you have any issues


## Contributing
Contributions are very welcome, I am not an expert on writing sh scripts so if you see any improvements, feel free to make a pull request.
For now this script only works with apt-get, if someone could teach me how to make the sh script compatible with other package managers, you would make my day. 
Suggestions for new zsh addons are also welcome but will only be added if they work for me, it is my personal zsh setup after all.

## Authors and acknowledgment
This is just a script that installs a collections of programs, none of which were made by me. 
Thank you creators of zsh and all addons!

- https://www.zsh.org/
- https://github.com/ohmyzsh/ohmyzsh
- https://github.com/romkatv/powerlevel10k
- https://github.com/zsh-users/zsh-autosuggestions
- https://github.com/zsh-users/zsh-syntax-highlighting

## License
MIT

## Project status
Will try to keep up to date with issues & pull requests if there are any. Will update it if I find new zsh plugins I like.
